package task1

import "fmt"

type Person struct {
	FirstName string
	LastName  string
	Age       int
}

func (p Person) Introduce() {
	fmt.Printf("Hey i am %s %s and i am %d years old\n", p.FirstName, p.LastName, p.Age)
}

func (p Person) IsEligibleToVote() {
	if p.Age >= 18 {
		fmt.Printf("Congratulations You are eligible to Vote\n")
	} else {
		fmt.Printf("You are not eligible to vote yet\n")
	}
}

func (p *Person) UpdateAge(new_Age int) {
	p.Age = new_Age
}
