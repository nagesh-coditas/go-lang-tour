package main

import (
	// "go-lang-tour/task1"
	"fmt"
	"go-lang-tour/task2"
)

func main() {

	//========Task1===========//
	// person := task1.Person{FirstName: "NAgesh", LastName: "Nagansur", Age: 25}
	// person.Introduce()
	// person.IsEligibleToVote()
	// person.UpdateAge(17)
	// person.IsEligibleToVote()
	//========Task1===========//

	//========Task2===========//

	emp1 := task2.Employee{
		EmpId:  1,
		Name:   "abc",
		Salary: 10000,
		Age:    20,
	}
	emp2 := task2.Employee{
		EmpId:  2,
		Name:   "def",
		Salary: 20000,
		Age:    25,
	}
	emp3 := task2.Employee{
		EmpId:  3,
		Name:   "ghi",
		Salary: 45000,
		Age:    27,
	}
	emp4 := task2.Employee{
		EmpId:  4,
		Name:   "jkl",
		Salary: 60000,
		Age:    30,
	}
	d1 := task2.Department{
		Name:      "HR",
		Employees: []task2.Employee{emp1, emp2},
	}
	d2 := task2.Department{
		Name:      "DotNet",
		Employees: []task2.Employee{emp3, emp4},
	}

	new_emp := task2.Employee{
		EmpId:  12,
		Name:   "efg",
		Salary: 20000,
		Age:    2,
	}

	//AverAge Salary in department
	fmt.Println("AvarAge Salary of Department D1:", d1.CalculateAverageSalaray())
	fmt.Println("AvarAge Salary of Department D2:", d2.CalculateAverageSalaray())

	//Adding new employee to department d1
	d1.AddEmployeeToDepartment(new_emp)
	for _, value := range d1.Employees {
		fmt.Printf("Employee Name :%s Salary: %d\n", value.Name, value.Salary)
	}

	//Giving raise to emp1
	fmt.Println("Salary before appraisal", emp1.Salary)
	emp3.Appraise(1000)
	fmt.Println("Salary after appraisal", emp1.Salary)

	//Deleting
	result, ok := d2.DeleteEmployee(10)
	fmt.Println(result)
	if ok != nil {
		fmt.Println("Failed to delete")
	} else {
		fmt.Println("Deleted successfully")
	}
	fmt.Println("Department D2", d2)
}
