package task2

import "errors"

type Employee struct {
	EmpId  int
	Name   string
	Age    int
	Salary int
}

type Department struct {
	Name      string
	Employees []Employee
}

func (d *Department) CalculateAverageSalaray() int {
	totalEmployee := len(d.Employees)
	totolSalary := 0
	for _, value := range d.Employees {
		totolSalary = totolSalary + value.Salary
	}
	averageSalary := totolSalary / totalEmployee
	return averageSalary
}

func (d *Department) AddEmployeeToDepartment(e Employee) {
	d.Employees = append(d.Employees, e)
	return
}

func (d *Department) DeleteEmployee(EmpId int) (bool, error) {
	for index, value := range d.Employees {
		if value.EmpId == EmpId {
			d.Employees = append(d.Employees[:index], d.Employees[index+1:]...)
			return true, nil
		}
	}
	return false, errors.New("Couldn't find provided EmpId")
}

func (e *Employee) Appraise(increment int) {
	e.Salary = e.Salary + increment
}
